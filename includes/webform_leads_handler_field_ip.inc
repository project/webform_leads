<?php

/**
* Field handler to provide simple renderer to convert the IP from a long to an IP.
*/
class webform_leads_handler_field_ip extends views_handler_field {
  function render($values) {
    if (!empty($values->webform_leads_sessions_ip))
      return long2ip($values->webform_leads_sessions_ip);
    else
      return NULL;
  }
}


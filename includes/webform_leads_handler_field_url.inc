<?php

/**
 * Field handler to manage the display of page urls.
 */
class webform_leads_handler_field_url extends views_handler_field {
  function render($values) {
    $page = $values->webform_leads_track_page;

    $dl = strpos($page, '/download/');
    if ($dl !== FALSE) {
      $page_path = substr($page, 0, $dl);
      $page_path = drupal_get_path_alias($page_path) . ' : Download';
    }
    else {
      $page_path = drupal_get_path_alias($page);
    }

    return $page_path;
  }
}


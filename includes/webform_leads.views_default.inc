<?php

function webform_leads_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'webform_leads_sessions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'webform_leads_sessions';
  $view->human_name = 'Webform Leads Sessions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Sessions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access webform lead reports';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'sid' => 'sid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'sid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No data was found for the provided Submission ID.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Webform Leads (Sessions): Session ID */
  $handler->display->display_options['fields']['session']['id'] = 'session';
  $handler->display->display_options['fields']['session']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['session']['field'] = 'session';
  /* Field: Webform Leads (Sessions): Session IP */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  /* Field: Webform Leads (Sessions): Session Creation Time */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  /* Field: Webform Leads (Sessions): Session Update Time */
  $handler->display->display_options['fields']['updated']['id'] = 'updated';
  $handler->display->display_options['fields']['updated']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['updated']['field'] = 'updated';
  $handler->display->display_options['fields']['updated']['date_format'] = 'time ago';
  /* Field: COUNT(Webform Leads (Page Visits): Page URL) */
  $handler->display->display_options['fields']['page']['id'] = 'page';
  $handler->display->display_options['fields']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['fields']['page']['field'] = 'page';
  $handler->display->display_options['fields']['page']['group_type'] = 'count';
  $handler->display->display_options['fields']['page']['label'] = 'Page Visits';

  /* Display: Registered Sessions */
  $handler = $view->new_display('page', 'Registered Sessions', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Webform Leads (Sessions): Session ID */
  $handler->display->display_options['fields']['session']['id'] = 'session';
  $handler->display->display_options['fields']['session']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['session']['field'] = 'session';
  $handler->display->display_options['fields']['session']['label'] = '';
  $handler->display->display_options['fields']['session']['exclude'] = TRUE;
  $handler->display->display_options['fields']['session']['element_label_colon'] = FALSE;
  /* Field: Webform Leads (Leads): name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'webform_leads';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Webform Leads (Leads): Email */
  $handler->display->display_options['fields']['email']['id'] = 'email';
  $handler->display->display_options['fields']['email']['table'] = 'webform_leads';
  $handler->display->display_options['fields']['email']['field'] = 'email';
  /* Field: COUNT(Webform Leads (Page Visits): Page URL) */
  $handler->display->display_options['fields']['page']['id'] = 'page';
  $handler->display->display_options['fields']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['fields']['page']['field'] = 'page';
  $handler->display->display_options['fields']['page']['group_type'] = 'count';
  $handler->display->display_options['fields']['page']['label'] = 'Page Visits';
  $handler->display->display_options['fields']['page']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['page']['alter']['path'] = 'admin/reports/leads/sessions/[session]';
  /* Field: COUNT(DISTINCT Webform Leads (Submissions): Submission ID) */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'webform_leads_submissions';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['sid']['label'] = 'Submissions';
  $handler->display->display_options['fields']['sid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sid']['alter']['path'] = 'admin/reports/leads/sessions/submissions/[session]';
  /* Field: Webform Leads (Sessions): Session Creation Time */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  /* Field: Webform Leads (Sessions): Session Update Time */
  $handler->display->display_options['fields']['updated']['id'] = 'updated';
  $handler->display->display_options['fields']['updated']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['updated']['field'] = 'updated';
  $handler->display->display_options['fields']['updated']['date_format'] = 'time ago';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Webform Leads (Page Visits): Page URL */
  $handler->display->display_options['filters']['page']['id'] = 'page';
  $handler->display->display_options['filters']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['filters']['page']['field'] = 'page';
  $handler->display->display_options['filters']['page']['operator'] = 'longerthan';
  $handler->display->display_options['filters']['page']['value'] = '3';
  /* Filter criterion: Webform Leads (Leads): Email */
  $handler->display->display_options['filters']['email']['id'] = 'email';
  $handler->display->display_options['filters']['email']['table'] = 'webform_leads';
  $handler->display->display_options['filters']['email']['field'] = 'email';
  $handler->display->display_options['filters']['email']['operator'] = 'longerthan';
  $handler->display->display_options['filters']['email']['value'] = '3';
  $handler->display->display_options['path'] = 'admin/reports/leads/sessions';

  /* Display: Page Visits */
  $handler = $view->new_display('page', 'Page Visits', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Page Visits';
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Webform Leads (Page Visits): Page URL */
  $handler->display->display_options['fields']['page']['id'] = 'page';
  $handler->display->display_options['fields']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['fields']['page']['field'] = 'page';
  /* Field: Webform Leads (Page Visits): Access Time */
  $handler->display->display_options['fields']['access_time']['id'] = 'access_time';
  $handler->display->display_options['fields']['access_time']['table'] = 'webform_leads_track';
  $handler->display->display_options['fields']['access_time']['field'] = 'access_time';
  $handler->display->display_options['fields']['access_time']['date_format'] = 'short';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Webform Leads (Page Visits): Access Time */
  $handler->display->display_options['sorts']['access_time']['id'] = 'access_time';
  $handler->display->display_options['sorts']['access_time']['table'] = 'webform_leads_track';
  $handler->display->display_options['sorts']['access_time']['field'] = 'access_time';
  $handler->display->display_options['sorts']['access_time']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Webform Leads (Page Visits): Session ID */
  $handler->display->display_options['arguments']['session']['id'] = 'session';
  $handler->display->display_options['arguments']['session']['table'] = 'webform_leads_track';
  $handler->display->display_options['arguments']['session']['field'] = 'session';
  $handler->display->display_options['arguments']['session']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['session']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['session']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['session']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['session']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'admin/reports/leads/sessions/%';

  /* Display: Anonymous Sessions */
  $handler = $view->new_display('page', 'Anonymous Sessions', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Sessions (including Anonymous)';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Webform Leads (Sessions): Session ID */
  $handler->display->display_options['fields']['session']['id'] = 'session';
  $handler->display->display_options['fields']['session']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['session']['field'] = 'session';
  $handler->display->display_options['fields']['session']['label'] = '';
  $handler->display->display_options['fields']['session']['exclude'] = TRUE;
  $handler->display->display_options['fields']['session']['element_label_colon'] = FALSE;
  /* Field: Webform Leads (Leads): name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'webform_leads';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Webform Leads (Leads): Email */
  $handler->display->display_options['fields']['email']['id'] = 'email';
  $handler->display->display_options['fields']['email']['table'] = 'webform_leads';
  $handler->display->display_options['fields']['email']['field'] = 'email';
  /* Field: COUNT(Webform Leads (Page Visits): Page URL) */
  $handler->display->display_options['fields']['page']['id'] = 'page';
  $handler->display->display_options['fields']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['fields']['page']['field'] = 'page';
  $handler->display->display_options['fields']['page']['group_type'] = 'count';
  $handler->display->display_options['fields']['page']['label'] = 'Page Visits';
  $handler->display->display_options['fields']['page']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['page']['alter']['path'] = 'admin/reports/leads/sessions/[session]';
  /* Field: COUNT(DISTINCT Webform Leads (Submissions): Submission ID) */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'webform_leads_submissions';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['sid']['label'] = 'Submissions';
  $handler->display->display_options['fields']['sid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sid']['alter']['path'] = 'admin/reports/leads/sessions/submissions/[session]';
  /* Field: Webform Leads (Sessions): Session Creation Time */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  /* Field: Webform Leads (Sessions): Session Update Time */
  $handler->display->display_options['fields']['updated']['id'] = 'updated';
  $handler->display->display_options['fields']['updated']['table'] = 'webform_leads_sessions';
  $handler->display->display_options['fields']['updated']['field'] = 'updated';
  $handler->display->display_options['fields']['updated']['date_format'] = 'time ago';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Webform Leads (Page Visits): Page URL */
  $handler->display->display_options['filters']['page']['id'] = 'page';
  $handler->display->display_options['filters']['page']['table'] = 'webform_leads_track';
  $handler->display->display_options['filters']['page']['field'] = 'page';
  $handler->display->display_options['filters']['page']['operator'] = 'longerthan';
  $handler->display->display_options['filters']['page']['value'] = '3';
  $handler->display->display_options['path'] = 'admin/reports/leads/sessions-anon';

  $views['webform_leads_sessions'] = $view;

  $view = new view();
  $view->name = 'webform_leads_submissions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'webform_submissions';
  $view->human_name = 'Webform Leads Submissions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'submitted' => 'submitted',
    'view_submission' => 'view_submission',
  );
  $handler->display->display_options['style_options']['default'] = 'submitted';
  $handler->display->display_options['style_options']['info'] = array(
    'submitted' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'view_submission' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results were found for the provided Session ID.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Webform submissions: Submitted */
  $handler->display->display_options['fields']['submitted']['id'] = 'submitted';
  $handler->display->display_options['fields']['submitted']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['submitted']['field'] = 'submitted';
  $handler->display->display_options['fields']['submitted']['date_format'] = 'short';
  /* Field: Webform submissions: View link */
  $handler->display->display_options['fields']['view_submission']['id'] = 'view_submission';
  $handler->display->display_options['fields']['view_submission']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['view_submission']['field'] = 'view_submission';
  $handler->display->display_options['fields']['view_submission']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_submission']['text'] = 'View Submission';
  /* Contextual filter: Webform Leads (Submissions): Session ID */
  $handler->display->display_options['arguments']['session']['id'] = 'session';
  $handler->display->display_options['arguments']['session']['table'] = 'webform_leads_submissions';
  $handler->display->display_options['arguments']['session']['field'] = 'session';
  $handler->display->display_options['arguments']['session']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['session']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['session']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['session']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['session']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Submissions';
  $handler->display->display_options['path'] = 'admin/reports/leads/sessions/submissions/%';

  $views['webform_leads_submissions'] = $view;

  return $views;
}

<?php

function webform_leads_views_data() {
  $data = array();

  $data['webform_leads_sessions']['table']['group'] = t('Webform Leads (Sessions)');
  $data['webform_leads_sessions']['table']['base'] = array(
    'field' => 'session',
    'title' => t('Session ID'),
    'help' => t('The Session ID of each unique visitor.'),
    'weight' => -10,
  );

  $data['webform_leads_sessions']['session'] = array(
    'title' => t('Session ID'),
    'help' => t('The Session ID of each unique visitor.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'session',
      'numeric' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_sessions']['ip'] = array(
    'title' => t('Session IP'),
    'help' => t('The IP associated with each session.'),
    'field' => array(
      'handler' => 'webform_leads_handler_field_ip',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_sessions']['created'] = array(
    'title' => t('Session Creation Time'),
    'help' => t('The creation time of the session.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_sessions']['updated'] = array(
    'title' => t('Session Update Time'),
    'help' => t('The update time of the session.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['webform_leads_track']['table']['group'] = t('Webform Leads (Page Visits)');
  $data['webform_leads_track']['table']['join']['webform_leads_sessions'] = array(
    'left_field' => 'session',
    'field' => 'session',
  );
  $data['webform_leads_track']['tid'] = array(
    'title' => t('Session tracking ID'),
    'help' => t('The Unique ID of this page visit.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_track']['session'] = array(
    'title' => t('Session ID'),
    'help' => t('The Session ID associated with this record.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'session',
      'numeric' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_track']['page'] = array(
    'title' => t('Page URL'),
    'help' => t('The page URL associated with this tracking record.'),
    'field' => array(
      'handler' => 'webform_leads_handler_field_url',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_track']['access_time'] = array(
    'title' => t('Access Time'),
    'help' => t('The time that this page was accessed.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['webform_leads_submissions']['table']['group'] = t('Webform Leads (Submissions)');
  $data['webform_leads_submissions']['table']['join']['webform_leads_sessions'] = array(
    'left_field' => 'session',
    'field' => 'session',
  );
  $data['webform_leads_submissions']['table']['join']['webform_submissions'] = array(
    'left_field' => 'sid',
    'field' => 'sid',
  );
  $data['webform_leads_submissions']['session'] = array(
    'title' => t('Session ID'),
    'help' => t('The Session ID associated with this record.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'session',
      'numeric' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads_submissions']['sid'] = array(
    'title' => t('Submission ID'),
    'help' => t('The webform submission ID associated with this record.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'sid',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );

  $data['webform_leads']['table']['group'] = t('Webform Leads (Leads)');
  $data['webform_leads']['table']['join']['webform_leads_sessions'] = array(
    'left_field' => 'session',
    'field' => 'session',
  );
  $data['webform_leads']['email'] = array(
    'title' => t('Email'),
    'help' => t('The email address for the lead.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_leads']['name'] = array(
    'title' => t('name'),
    'help' => t('The name of the lead.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  return $data;
}


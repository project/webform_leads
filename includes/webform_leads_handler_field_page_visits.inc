<?php

/**
* Field handler to count page visits for the associated session.
*/
class webform_leads_handler_field_page_visits extends views_handler_field_numeric {
  function render($values) {
    if (!empty($values->session)) {
      $count = db_select('webform_leads_track', 't')
        ->fields('t')
        ->condition('session', $values->session)
        ->execute()
        ->rowCount();

      return $count;
    }
    else {
      return NULL;
    }
  }
}

